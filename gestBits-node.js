/*
nom:gestBitsnode-0.1-test.js
version node 


*/
TgestBits=function()
	{
	//affiche l'etat des bits d'une valeur n ou l'etat d'un bit
	this.showBit=function(n,b)
		{
		bit=[];
		bit[7]=(n&128)?1:0;
		bit[6]=(n&64)?1:0;
		bit[5]=(n&32)?1:0;
		bit[4]=(n&16)?1:0;
		bit[3]=(n&8)?1:0;
		bit[2]=(n&4)?1:0;
		bit[1]=(n&2)?1:0;
		bit[0]=(n&1)?1:0;
		if(b>=0&&b<=7){out=bit[b];}
		else{out= bit[7]+' '+bit[6]+' '+bit[5]+' '+bit[4]+' '+bit[3]+' '+bit[2]+' '+bit[1]+' '+bit[0]+' ';}
		return out;
		}

	this.setBit=function(n,bit){return n | Math.pow(2,bit);}//ne met que le bit a 1 ne touche pas les autres
//	this.setBit=function(n,bit){n |=(bit << n);return n}
	this.invBit=function(n,bit){return n ^ Math.pow(2,bit);}//inverse la valeur du bit
 	this.notBit=function(n){return ~n;}
 	this.clrBit=function(n,bit)	// met ts a 1 sauf bit specifier mis a zero
		{
		bits=[];
		bits[7]=(bit==7)?0:(n&128)?1:0;
		bits[6]=(bit==6)?0:(n&64)?1:0;
		bits[5]=(bit==5)?0:(n&32)?1:0;
		bits[4]=(bit==4)?0:(n&16)?1:0;
		bits[3]=(bit==3)?0:(n&8)?1:0;
		bits[2]=(bit==2)?0:(n&4)?1:0;
		bits[1]=(bit==1)?0:(n&2)?1:0;
		bits[0]=(bit==0)?0:(n&1)?1:0;
		return bits[7]*Math.pow(2,7)+bits[6]*Math.pow(2,6)+bits[5]*Math.pow(2,5)+bits[4]*Math.pow(2,4)+bits[3]*Math.pow(2,3)+bits[2]*Math.pow(2,2)+bits[1]*Math.pow(2,1)+bits[0]*Math.pow(2,0);
		}
	}
//activer en tant que module node.js
try{exports.gestBits=new TgestBits();}finally{}
