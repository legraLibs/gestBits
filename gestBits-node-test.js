#!/usr/bin/env node
/*
nom: gestBits-0.1-node-test.js
//https://npmjs.org/package/parport
//https://code.google.com/p/parallel-port/
//http://www.unixgarden.com/index.php/gnu-linux-magazine-hs/programmation-du-port-parallele

//install le module parport
//   #npm install parport
*/

console.log('**************** gestBits-0.1-node-test.js ********************************');

erreur=null;
console.log('Chargement de la librairie: gestBits');

try	{
	var gestBits=require('/www/sites/intersites/lib/perso/js/gestBits/gestBits-0.1-node.js').gestBits;
	}
catch(err)
	{
	console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
	console.log('erreur au chargement de la librairie gestBits: '+err);
	console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
	erreur=err;
	return;
	}
console.log('erreur:'+erreur);
if(!erreur){console.log('charger');}
	

//var par = require('/lib/node_modules/parport');

var n= 64+32;
console.log('affichage de tous les bits de n');
console.log('showBit('+n+'):'+gestBits.showBit(n));

b=6
console.log('affichage du bit '+b+' de n');
console.log('showBit('+n+','+b+'):'+gestBits.showBit(n,b));


///////////////////////////////
b=2;
console.log('setBit('+n+','+b+'):');
n=gestBits.setBit(n,b);
console.log('showBit('+n+'):'+gestBits.showBit(n));

////////////////////////////////
b=5;
console.log('invBit('+n+','+b+'):');
n=gestBits.invBit(n,b);
console.log('showBit('+n+'):'+gestBits.showBit(n));

////////////////////////////////
console.log('notBit('+n+'):');
n=gestBits.notBit(n);
console.log('showBit('+n+'):'+gestBits.showBit(n));

////////////////////////////////
b=7;
console.log('notBit('+n+','+b+'):');
n=gestBits.clrBit(n,b);
console.log('showBit('+n+'):'+gestBits.showBit(n));

console.log('************************************************');


